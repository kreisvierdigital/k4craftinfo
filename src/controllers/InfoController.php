<?php


namespace k4\k4craftinfo\controllers;


use Craft;
use craft\helpers\DateTimeHelper;
use craft\helpers\Db;
use craft\web\Controller;
use craft\web\Response;
use k4\k4craftinfo\K4CraftInfo;
use Kint\Kint;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class InfoController extends Controller
{
    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index'];

    public $enableCsrfValidation = true;

    public function actionIndex(){

        $key = Craft::$app->request->getQueryParam('key');
        $format = Craft::$app->request->getQueryParam('format');
        $settingsKey = K4CraftInfo::getInstance()->getSettings()->key;

        if ( empty($key) ) return $this->asErrorJson('forbidden');
        if ( empty($settingsKey) ) return $this->asErrorJson('forbidden');
        if ( $key != $settingsKey ) return $this->asErrorJson('forbidden');

        $updateService = Craft::$app->getUpdates();

        $result = array(
            "craft-version" => Craft::$app->version,
            "critical-updates-available" => $updateService->getIsCriticalUpdateAvailable(),
            "cms-updates" => $this->getCmsUpdates($updateService),
            "plugin-updates" => $this->getPluginUpdates($updateService),
            "plugins" => $this->getPlugins()
        );

        switch ($format) {
            case 'json':
                return $this->asJson($result);
            case 'xml':
                return $this->asXml($result);
            default:
                return $this->asJson($result);
        }

    }



    public function getCmsUpdates($updateService)
    {
        $allUpdates = $updateService->getUpdates();

        if (count($allUpdates->cms->releases) > 0 ){

            return array_map(function($release) {
                return $release->version;
            }, $allUpdates->cms->releases);
        }
        else {
            return [];
        }
    }

    public function getPluginUpdates($updateService)
    {
        $allUpdates = $updateService->getUpdates();

        if (count($allUpdates->plugins) > 0 )
        {
            array_walk($allUpdates->plugins, function(&$value,$key) {
                $value = array_map(function($release){
                    return $release->version;
                },$value->releases);
            });

            return array_filter($allUpdates->plugins,function($plugin){
                return ( is_array($plugin) && count($plugin) > 0 );
            });
        }
        else {
            return [];
        }
    }

    public function getPlugins()
    {
        $allInfo = Craft::$app->getPlugins()->getAllPluginInfo();

        if (count($allInfo) > 0 )
        {
            array_walk($allInfo, function(&$value,$key) {
                $value = $value["version"];
            });

            return $allInfo;
        }
        else {
            return [];
        }
    }




}