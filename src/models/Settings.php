<?php

namespace k4\k4craftinfo\models;


use craft\base\Model;

class Settings extends Model
{

    public $key;

    public function rules()
    {
        return [
            [['key'], 'required']
        ];
    }

}