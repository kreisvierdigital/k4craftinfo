<?php


namespace k4\k4craftinfo;


use Craft;
use craft\base\Plugin;
use craft\events\RegisterUrlRulesEvent;
use craft\events\RegisterUserPermissionsEvent;
use craft\services\UserPermissions;
use craft\web\UrlManager;
use k4\k4craftinfo\models\Settings;
use yii\base\Event;

/**
 * Class K4CraftInfo
 *
 * @package k4\k4craftinfo
 *
 */
class K4CraftInfo extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var K4CraftInfo
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    public $hasCpSection = false;

    public $pluginHandle = 'k4-craft-info';

    public $hasCpSettings = true;


    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->registerServices();
        $this->registerRoutes();
        $this->registerPermissions();
        $this->registerVariable();


    }


    // Settings
    // =========================================================================


    // CP Navigation
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getCpNavItem()
    {
        $currentUser = Craft::$app->getUser()->getIdentity();
        $allowAdminChanges = Craft::$app->getConfig()->getGeneral()->allowAdminChanges;

        $cpNavItem =  parent::getCpNavItem();
        $cpNavItem['subnav'] = [];

        $cp_nav = include __DIR__ . '/nav_cp.php';

        foreach($cp_nav as $key => $item){

            $cpNavItem['subnav'][$key] = $item;

        }

        return $cpNavItem;
    }


    // Own Helper Functions
    // =========================================================================


    protected function registerServices(){
        // Register services as components
        $this->setComponents([

        ]);
    }

    protected function registerRoutes()
    {
        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $routes       = include __DIR__ . '/routes_site.php';
                $event->rules = array_merge($event->rules, $routes);
            }
        );

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $routes       = include __DIR__ . '/routes_cp.php';
                $event->rules = array_merge($event->rules, $routes);

                $routes  = include __DIR__ . '/routes_test.php';
                $event->rules = array_merge($event->rules, $routes);
            }
        );
    }

    protected function registerPermissions(){

        // Register user permissions
        Event::on(
            UserPermissions::class,
            UserPermissions::EVENT_REGISTER_PERMISSIONS,
            function(RegisterUserPermissionsEvent $event) {
                $event->permissions['k4-craft-info'] = include __DIR__ . '/permissions.php';
            }
        );

    }

    protected function registerVariable(){

    }

    protected function createSettingsModel()
    {
        return new Settings();
    }

    protected function settingsHtml()
    {
        return \Craft::$app->getView()->renderTemplate('k4-craft-info/settings', [
            'settings' => $this->getSettings()
        ]);
    }


}